"""A setuptools based setup module for Shape Retrieval Framework.
"""

import sys
from os import path
from setuptools import setup, find_packages


def _forbid_publish():
    argv = sys.argv
    blacklist = ['register', 'upload']
    
    for command in blacklist:
        if command in argv:
            values = {'command': command}
            print('Command "%(command)s" has been blacklisted, exiting...' %
                  values)
            sys.exit(2)


_forbid_publish()

HERE = path.abspath(path.dirname(__file__))

REQUIREMENTS = ['shrkit', 'shapelab']


setup(
    name='explib',
    version='0.0.1',
    description='User trial helper library',
    url='https://gitlab.com/shrkit/user-trial-experiments/',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5'
    ],
    packages=find_packages(),
    install_requires=REQUIREMENTS,
    extras_require={
        'dev': ['pylint',
                'autopep8',
                'ipdb',
                'jedi'],
    })
