"""Trial task main logic. Not need to be understood by the
participant.

"""

import os.path as osp
from itertools import repeat
from multiprocessing import Pool
import pickle
from collections import Counter

import numpy as np
import numpy.random as npr
import sklearn.cluster
from scipy.spatial.distance import cosine, euclidean
from tqdm import tqdm

import shapelab.igl
import shrkit.feature.thirdparty.laplace_beltrami as laplace_beltrami
import shrkit.database
from shrkit.ranking.metricranking import MetricRanking
from shrkit.ranking.metrics import MetricType

# pylint: disable=too-few-public-methods

def _extract_lbo(idx_row):
    _, row = idx_row
    verts, faces = shapelab.igl.read3DObject(row.content)
    stiffness, mass = laplace_beltrami.computeAB(verts, faces)
    return row, stiffness, mass


def compute_lbos(output_db, dataset, jobs=6):
    """Compute the Laplace–Beltrami operator for mesh dataset.

    Args:

        output_db (str): Output database
        dataset (:obj:`Pandas.DataFrame`): The input dataset
        jobs (int): How many processes to use.

    Returns:
        :obj:`shrkit.database.LMDBFeaturesDB`: The LBO database handle.
    """
    map_func = map
    if jobs > 1:
        pool = Pool(processes=jobs)
        map_func = pool.imap_unordered

    mapping = map_func(_extract_lbo, dataset.iterrows())

    db_env = shrkit.database.LMDBFeaturesDB.open(output_db)
    with db_env.begin(True, 10) as txn:
        for row, stiffness, mass in tqdm(
                mapping, total=len(dataset),
                desc="Computing Laplace–Beltrami Operator: {}".format(
                    osp.basename(output_db))):
            txn.put(row.model_id, row.category, (stiffness, mass))
    return db_env


def load_lbos(input_db):
    """Loads the feature database handle.

    Args:

        input_db (str): The input database path.

    Returns:
        :obj:`shrkit.database.LMDBFeaturesDB`: The LBO database handle.
    """
    db_env = shrkit.database.LMDBFeaturesDB.open(input_db)
    return db_env


def _extract_feature(item):
    feature_extractor, idx_row = item
    _, row = idx_row
    feat = feature_extractor.predict_from_lbo(row.content)
    return row, feat


def compute_features(output_db, lbo_database, feature_extractor, jobs=6):
    """Given the applied LBO database, extract features from it.

    Args:

        output_db (str): The output database path.

        lbo_database (:obj:`shrkit.database.LMDBFeaturesDB`): The LBO
         database handle.

        feature_extractor (object): A feature extractor instance. See
         :obj:`shrkit.feature.shapedna.ShapeDNA` and
         :obj:`shrkit.feature.hks.HKS`

        jobs (int): How many processes to use.

    Returns:
        :obj:`shrkit.database.LMDBFeaturesDB`: The feature database handle.
    """

    map_func = map
    if jobs > 1:
        pool = Pool(processes=jobs)
        map_func = pool.imap_unordered

    lbo_df = lbo_database.to_dataframe()
    mapping = map_func(
        _extract_feature,
        zip(repeat(feature_extractor), lbo_df.iterrows()))

    feat_env = shrkit.database.LMDBFeaturesDB.open(output_db)
    with feat_env.begin(True, 10) as feat_txn:
        for row, feat in tqdm(
                mapping, total=len(lbo_df),
                desc="Extracting features into: {}".format(
                    osp.basename(output_db))):
            feat_txn.put(row.model_id, row.category, feat)

    return feat_env


def load_features(input_db):
    """Loads the feature database handle.

    Args:

        input_db (str): The input database path.

    Returns:
        :obj:`shrkit.database.LMDBFeaturesDB`: The feature database handle.
    """
    return shrkit.database.LMDBFeaturesDB.open(input_db)


def create_ranker(feature_db, distance):
    """Creates a ranker object.

    Args:

        feature_db (:obj:`shrkit.database.LMDBFeaturesDB`): The
         feature database handle. Those are the features retrieved.

    Returns:

        :obj:`shrkit.ranking.metricranking.MetricRanking`: Ranker
         object. Use ranker.rank(query_feature).
    """

    if distance == "cosine":
        dist_func = cosine
    elif distance == "euclidean":
        dist_func = euclidean
    else:
        raise "Unknown distance {}".format(distance)

    ranker = MetricRanking(dist_func, MetricType.Distance)
    ranker.fit(feature_db.to_dataframe())
    return ranker


def run_queries(output_file, ranker, features, top_k=100):
    """Query all the given features with a given ranker.

    Args:

        output_file (str): Reentrant output file path.

        ranker (:obj:`shrkit.ranking.metricranking.MetricRanking`):
         The ranking object.

       features (:obj:`shrkit.database.LMDBFeaturesDB`): The query features.

       top_k (int): How many items to retrieve.

    Returns:

        List[(int, List[:obj:`shrkit.ranking.Ranking`])]: Tuples per
         query features. Each one contain the category and its
         ranking. The ranking is a list containg the retrieval
         information. The length of the inner list will be `top_k`.
    """
    query_rankings = []
    query_features = features.to_dataframe()

    for row in tqdm(query_features.itertuples(), total=len(query_features),
                    desc="Ranking 3D features to: {}".format(
                        osp.basename(output_file))):
        ranking = ranker.rank(row.content)
        query_rankings.append(
            (row.category, ranking[:top_k]))

    with open(output_file, 'wb') as stream:
        pickle.dump(query_rankings, stream)

    return query_rankings


class BagOfWordsModel:
    """Bag of words models.

    Attributes:

        kmeans (:obj:`sklearn.cluster.KMeans`): The fitted kmeans used
         to extract centroids based histogram. Other K-Means versions
         of sklearn's are compatible.

    """

    def __init__(self, kmeans):
        self.kmeans = kmeans

    def predict(self, local_features):
        """Given a set of local_features, aggregates them into a global
        vectors.

        Args:

            local_features (:obj:`numpy.Array`): A NxMxV shape
             array. The first axis are the source object. `M` are the
             number of local features, and `V` is the local feature
             dimension.

        Returns:

            :obj:numpy.Array: A NxU shape array. `N` are the source
            object, and `U` is the global feature dimension, or the
            number of centroids.
        """

        clusters = self.kmeans.predict(local_features)

        codeword = np.zeros((self.kmeans.n_clusters, 1), np.int32)
        counter = Counter(clusters)
        for cluster, count in counter.items():
            codeword[cluster] = count

        return codeword


def fit_bag_of_words(output_file, feature_database, num_clusters, sample_per_objs):
    """Fit a K-Means based bag-of-words (BoW) model, and write it to
    pickle file.

    Args:

        output_file (str): Pickle output file name

        feature_database (:obj:`shrkit.database.LMDBFeaturesDB`): Local
         features database handle.

        num_clusters (int): Number of BoW centroids.

        sample_per_objs (int): Number of local features to sample per objects.

    Returns:
        :obj:`BagOfWordsModel`: The fitted bag of words model.
    """

    all_features = []
    for _, __, feature in feature_database.begin():
        sel = npr.choice(feature.shape[0], sample_per_objs)
        feature = feature[sel, :]
        all_features.append(feature)

    kmeans = sklearn.cluster.MiniBatchKMeans(n_clusters=num_clusters,
                                             verbose=False)
    all_features = np.vstack(all_features)
    print("--[ Fitting bag-of-words")
    kmeans = kmeans.fit(all_features)
    with open(output_file, 'wb') as stream:
        pickle.dump(kmeans, stream)

    predictor = BagOfWordsModel(kmeans)
    return predictor


def load_bag_of_words(input_file):
    """Loads the bag-of-words model from a input file.

    Args:

        input_file (str): The input file path.

    Returns:
        :obj:`BagOfWordsModel`: The fitted bag of words model.
    """
    with open(input_file, 'rb') as stream:
        return BagOfWordsModel(pickle.load(stream))


def compute_codebook(output_db, features, bow_model):
    """Computes the BoW codebook.

    Args:

        output_db (str): The output codebook database.

        features (:obj:`shrkit.database.LMDBFeaturesDB`): The local
         features database handle.

        bow_model (:obj:`BagOfWordsModel`): A fitted bow model.

    Returns:
        :obj:`shrkit.database.LMDBFeaturesDB`: The codebook database handle.
    """

    feat_env = shrkit.database.LMDBFeaturesDB.open(output_db)
    with feat_env.begin(True, 10) as feat_txn:
        for model_id, category, feature in tqdm(
                features.begin(),
                total=len(features),
                desc="Generating codebook: {}".format(
                    osp.basename(output_db))):
            codeword = bow_model.predict(feature)
            feat_txn.put(model_id, category, codeword)
    return feat_env


def load_codebook(input_db):
    """Loads the codebook.

    Args:

        input_file (str): The input file path.

    Returns:
        :obj:`shrkit.database.LMDBFeaturesDB`: The codebook database handle.
    """
    return shrkit.database.LMDBFeaturesDB.open(input_db)
