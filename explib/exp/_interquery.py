"""Trial nodes
"""

import os.path as osp
from collections import Counter

import numpy as np
import cv2

import shrkit.workflow as srwf
from shrkit.ranking import Ranking
import shrkit.viz


class InteractiveQuery(srwf.Interface):
    """This node exibits a window showing the retrieved objects.

    Args:

        feature_extractor (object): An object with `.predict` function.
         It accepts an input filepath and outputs a feature vector.

        ranker (object): An object with `.rank()` function. Accepts a
         feature vector and ouputs a ranking.

       retrieval_df (:obj:`shrkit.database.DataFrame`): Data frame cont
    """

    def evaluate(self, feature_extractor, ranker, retrieval_dataset, input_model, codewords=None):
        feature = feature_extractor.predict(input_model)
        if codewords is not None:
            feature = codewords.predict(feature)

        modelid_to_filepath = dict(
            zip(retrieval_dataset.model_id, retrieval_dataset.content))
        ranking = ranker.rank(feature)

        rank_model_files = []
        for rank in ranking[:10]:
            rank_model_files.append(
                Ranking(modelid_to_filepath[rank.model_id], rank.score, -1))

        image = shrkit.viz.snapshot_query(input_model, rank_model_files)
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        cv2.imshow("Retrieval", image)
        cv2.waitKey()
