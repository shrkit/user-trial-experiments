{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to Create Workflows with Shrkit\n",
    "\n",
    "This tutorial shows how the Shrkit workflow module can help to develop scientific-computational experiments.\n",
    "\n",
    "Consider the hypothetical routine of computing the frequency of words from a text file. It is a simplification of a typical stage in machine learning, natural language processing, or even bioinformatics. Although simple, it summarizes two practical problems addressed by researchers:\n",
    "\n",
    "* How to reproduce and share experiments.\n",
    "* How to do experiments based on interchangeable parts.\n",
    "\n",
    "In this example, the first step taken by many developers would to start with the main logic for counting:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_word_freq(input_filepath):\n",
    "    \"\"\"Compute word frequency from a input text file.\n",
    "    \n",
    "    Args:\n",
    "        input_filepath (str): Input text file path.\n",
    "           \n",
    "    Returns:\n",
    "        Dict[str: int]: Each key is a word, and its \n",
    "         values are their frequency.\n",
    "    \"\"\"\n",
    "\n",
    "    word_freq = {}\n",
    "\n",
    "    # Count the words\n",
    "    with open(input_filepath, 'r') as stream:\n",
    "        for line in stream:\n",
    "            for word in line.split():\n",
    "                word_freq[word] = word_freq.get(word, 0) + 1\n",
    "\n",
    "    return word_freq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above function returns a dictionary with the count of every word. It's run as follows on the `big-text.txt` text file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "word_freq = compute_word_freq('big-text.txt')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An essential part of any experimentation is visualizing stage results. A researcher could write the following code to show the `K` most frequent words:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_kmost(word_freq, K=5):\n",
    "    \"\"\"Prints the K most frequent words.\n",
    "    \n",
    "    Args:\n",
    "        word_freq (Dict[str: int]): Each key is a word, \n",
    "         and its values are its counting.\n",
    "        K (int): Specify how many words to show.\n",
    "    \"\"\"\n",
    "\n",
    "    ordered_words = sorted(word_freq.items(),\n",
    "                           key=lambda item: item[1],\n",
    "                           reverse=True)\n",
    "    for i, (word, count) in enumerate(ordered_words):\n",
    "        if i == K:\n",
    "            break\n",
    "        print('{}-nth word: \"{}\" happens {} times'.format(\n",
    "              i, word, count))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then the complete code of this small experiment becomes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "word_freq = compute_word_freq('big-text.txt')\n",
    "print_kmost(word_freq, K=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Actual experiments, as typically found in Deep Learning or Big-data, may take hours or days.\n",
    "\n",
    "In order not to perform processing unnecessarily, a common strategy is to leave the experiment reentrant. That is, saving intermediate results in files, and load them in future executions.\n",
    "\n",
    "Continuing with this idea, routines to save and load data can be like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def save_freq(word_freq, output_filepath):    \n",
    "    with open(output_filepath, 'w') as stream:\n",
    "        for word, cont in word_freq.items():\n",
    "            stream.write('{} {}\\n'.format(word, cont))\n",
    "            \n",
    "def load_freq(input_filepath):\n",
    "    with open(input_filepath, 'r') as stream:\n",
    "        word_freq = {}\n",
    "        for line in stream:\n",
    "            word, cont = line.split()\n",
    "            word_freq[word] = int(cont)\n",
    "    return word_freq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The naive way of joining the steps is to check for the existence of the generated files manually:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "def exp1():\n",
    "    if not os.path.exists(\"big-text-freq.txt\"):\n",
    "        word_freq = compute_word_freq(\"big-text.txt\")\n",
    "        save_freq(word_freq, \"big-text-freq.txt\")\n",
    "    else:\n",
    "        word_freq = load_freq(\"big-text-freq.txt\")\n",
    "\n",
    "    print_kmost(word_freq, 10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For demonstration pruposes, assure that \n",
    "# the frequency file doesn't exists.\n",
    "import os\n",
    "\n",
    "if os.path.exists('big-text-freq.txt'):\n",
    "    os.remove('big-text-freq.txt')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "exp1()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now rerunning the experiment, the processing time will undergo a drastic reduction:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "exp1()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that reading the results of a file was at least 2 times faster than re-computing again.\n",
    "\n",
    "However, actual experiments contain multiple stages, and it would not be productive to repeat the reentrant logic code every time. More importantly, if parameters changes, then the affected parts or dependencies need to be rerun. For example, `compute_word_freq` stage depends on the data of 'big.txt'. Dependency means that if the file \"big.txt\" is modified, then the count must be (re)done even if its intermediary exists.\n",
    "\n",
    "Tools like Make, Luigi, Weka, and our Shrkit, encapsulate this check by modeling experiment as a direct acyclic graph (DAG). With experiments being viewed as graphs, the computations and data become the nodes, and their dependencies become the edges.\n",
    "\n",
    "Shrkit also represents experiments as DAG. We'll start a graph with Shrkit; then we'll define a node for computing word frequencies, and add it to the DAG. \n",
    "\n",
    "One way to get a DAG object is to call `shrkit.workflow.get_graph`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import shrkit.workflow as srwf\n",
    "exp1 = srwf.get_graph('exp1')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will create a DAG named `'exp1'`. On Shrkit, any DAG can be retrieved from the `get_graph` function.\n",
    "\n",
    "To define nodes, users should inherit from the class `shrkit.workflow.Interface`. The derived classes must implement a `evaluate()` function, and a optional `load()`. The first one is where the main logic should put, and save its intermediary results. The second one, `load()`, should keep the node reentrant, by loading the previously computed value. \n",
    "\n",
    "The node type `WordFrequency` will carry previous word frequency functionality of computing and load:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class WordFrequency(srwf.Interface):\n",
    "    \"\"\"Compute word frequencies from a input text file.\n",
    "    \"\"\"\n",
    "\n",
    "    def evaluate(self, resource, input_resource):\n",
    "        \"\"\"Args:\n",
    "\n",
    "            resource (skrkit.workflow.FSResource): Reentrancy checkpoint. \n",
    "\n",
    "            input_resource (skrkit.workflow.FSResource): Resource pointing to \n",
    "                the input text file path.\n",
    "             \n",
    "        Returns:\n",
    "            Dict[str: int]: Each key is a word, and values are its\n",
    "             counting.\n",
    "        \"\"\"\n",
    "        \n",
    "        word_freq = compute_word_freq(input_resource.filepath)        \n",
    "        save_freq(word_freq, resource.filepath)\n",
    "        return word_freq\n",
    "\n",
    "    def load(self, resource):\n",
    "        \"\"\"Loads the data from previous `evaluate()` call.\n",
    "\n",
    "        Args:\n",
    "            resource (skrkit.workflow.FSResource): Reentrancy checkpoint. \n",
    "\n",
    "        Returns:\n",
    "            Dict[str: int]: Each key is a word, and values are its\n",
    "             counting.\n",
    "        \"\"\"\n",
    "\n",
    "        return load_freq(resource.filepath)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above code needs some clarications.\n",
    "    \n",
    "As early stated, Shrkit can track changes of nodes inputs. To track changes that occur inside external entities such files or database entry, Shrkit provides classes derived from `shrkit.workflow.Resource` which encapsulates existence testing and content representation. \n",
    "\n",
    "The `evaluate()` function, calls the main logic and save the results. Node's `evaluate()` function may take any number of arguments. In the above case, it accepts two arguments: `resource` and `input_resource`. The `resource` name is a special one for Shrkit. Arguments named as this are expected to be a `Resource` type. It tells to Shrkit that the node is reentrant, and its value must refer to an intermediary checkpoint destination. The code refers to a `filepath` attribute, meaning that `WordFrequency` expects a file system resource - a `shrkit.workflow.FSResource` instance.\n",
    "\n",
    "The second argument is also expected to be a `shrkit.workflow.FSResource`. So Shrkit will detect changes on the file content, and rerun the `evaluate()` if they occur.\n",
    "\n",
    "The `load()` function is called whatever Shrkit judges that no alteration occurred to the node's inputs. So it can be loaded.\n",
    "\n",
    "Until now, only the node definition were shown. To put the class into use, it must be instantiated and added to a DAG:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp1.word_freq = WordFrequency(srwf.FSResource('big-text-freq.txt'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A node is added by instancing and assigning it to a DAG attribute. For those not familiar with Python, new attributes can be added to an object on the fly. Every instanced node have a unique name given by its attribute name, as `word_freq` in the previous line. This name can be referenced to invoke it from command-line.\n",
    "\n",
    "The constructor argument, `srwf.FSResource('big-text-freq.txt')`, is the `resource` argument value. The `resource` parameter is so special that it be can passed directly on the constructor. Following its node definition, it's a file system resource.\n",
    "\n",
    "Other arguments are assigned to nodes by using its `.args` attribute. By reflection, Shrkit fills `.args` with attributes named after each  `evaluate()` arguments. For example, to set `input_resource` pointing to the 'big-text.txt' file system resource:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp1.word_freq.args.input_resource = srwf.FSResource('big-text.txt')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To execute any node, invoke its `call()` method instead of `evaluate()` or `load()`. This function will verify the reentracy conditions, and use the appropiate method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "word_freq = exp1.word_freq.call()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `call()` method returns the same as `evaluate()` (or `load()`). \n",
    "\n",
    "Let's keep building the rest of the experiment, now by defining a node to show words."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class PrintKMost(srwf.Interface):\n",
    "    \"\"\"Prints the K most frequent words.    \n",
    "    \"\"\"\n",
    "\n",
    "    def evaluate(self, word_freq, K):   \n",
    "        \"\"\"Args:\n",
    "        word_freq (Dict[str: int]): Each key is a word, \n",
    "         and its values are their frequency.\n",
    "         \n",
    "        K (int): Specify how many words to show.\n",
    "        \"\"\" \n",
    "\n",
    "        ordered_words = sorted(word_freq.items(),\n",
    "                               key=lambda item: item[1],\n",
    "                               reverse=True)\n",
    "        for i, (word, count) in enumerate(ordered_words):\n",
    "            if i == K:\n",
    "                break\n",
    "            print('{}-nth word: \"{}\" happens {} times'.format(\n",
    "                  i, word, count))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `PrintKMost` node is a simpler one. As it doesn't produce any intermediary results, so no do need to have a `resource` argument. Differently from the `WordFrequency`, it accepts common python object like the `word_freq` dict or the `K` integer.  \n",
    "\n",
    "Adding to the graph:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp1.kmost_freq = PrintKMost()\n",
    "with exp1.kmost_freq as args:    \n",
    "    args.word_freq = exp1.word_freq\n",
    "    args.K = 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a shortcut to `args` attribute, users may use the `with` statement to not repeat the `graph.vertice.args.` pattern. \n",
    "\n",
    "When setting `args.word_freq = exp1.word_freq` argument, we create a edge from the `exp1.word_freq` node to `exp1.kmost_freq`.  The return value of `exp1.word_freq` will be send to the `word_freq` argument of `kmost_freq`. The `K` argument was set to a constant value.\n",
    "\n",
    "Like for `word_freq`, the `kmost_freq` is executed with `call()`. Besides checking reentrancy, the call method resolves dependency call, executing only the need nodes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp1.kmost_freq.call()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In that call, `exp1.word_freq` was evaluated. The next example will clean the middle node, and rerun:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp1.word_freq.clear()\n",
    "exp1.kmost_freq.call()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Executing it again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Shrkit caches previous values. \n",
    "# Use `redo=True` to force the call\n",
    "# node execution\n",
    "exp1.kmost_freq.call(redo=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The `.args` value restrictions\n",
    "\n",
    "Shrkit writes all arguments to a database, so every node instance has a signature of its previous call. If the signature matches with the stored one, the node is considered updated. However, constant values such `kmost_freq.args.K = 10` must have two restrictions:\n",
    "\n",
    "* Must be serializable with `pickle`;\n",
    "* Must have a `__equal__` implementation.\n",
    "\n",
    "Those two restrictions are needed to compare and write the signatures. Suppose that we're using the following `Uncomparable` class, that does not have a `__eq__` method:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Uncomparable:\n",
    "    def __init__(self, a):\n",
    "        self.a = a\n",
    "\n",
    "    def __str__(self):\n",
    "        return str(self.a)                "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Uncomparable objects happens mostly with C/C++ wrappers or too big to compare objects. If we try to add to the following DAG:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "class Print(srwf.Interface):\n",
    "    def evaluate(self, obj):\n",
    "        print(obj)\n",
    "        \n",
    "ta = srwf.get_graph('test-args', overwrite=True)\n",
    "ta.print_const = Print()\n",
    "try:\n",
    "    ta.print_const.args.obj = Uncomparable(4)\n",
    "except srwf.WorkflowError as error:\n",
    "    print(\"Error: {}\".format(error))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An error is raised because `Uncomparable` does not have a `__eq__` override. \n",
    "\n",
    "To actually add it to a graph is necessary to encapsulate inside a node, so Shrkit can compare its signature. \n",
    "\n",
    "One way is to use the `shrkit.workflow.VarNode` class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ta.print_const.args.obj = srwf.VarNode(Uncomparable(4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a downside,  `Uncomparable` will always be created. To overcome this, we can create a node to initialize it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class GetUncomparable(srwf.Interface):\n",
    "    def evaluate(self, int_value):\n",
    "        return Uncomparable(int_value)\n",
    "    \n",
    "ta.get_uncomp = GetUncomparable()\n",
    "ta.get_uncomp.args.int_value = 4\n",
    "\n",
    "ta.print_const.args.obj = ta.get_uncomp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ta.print_const.call()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The later way advantage is that the construction arguments are tracked."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Viewing Graphs\n",
    "\n",
    "Shrkit graphs can be view with Graphviz:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import graphviz\n",
    "import shrkit.workflow.viz as viz\n",
    "\n",
    "graphviz.Source(viz.dag2dot(exp1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Adding more nodes\n",
    "\n",
    "Suppose that a new thesis arrived about random texting counting. Researchers want to base their new experiments on the former base, so they know that are using the same procedures on both thesis.\n",
    "\n",
    "With Shrkit, they could define a new graph vertex for generating random text and insert into a new graph.\n",
    "\n",
    "First, the next code will define the random text generator interface:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import random\n",
    "\n",
    "class RandomText(srwf.Interface):\n",
    "    \"\"\"Generates random text.\n",
    "    \"\"\"\n",
    "\n",
    "    def evaluate(self, resource, num_of_words, selected_words):\n",
    "        \"\"\"Args:\n",
    "\n",
    "            resource (shrkit.workflow.FSResource): File resource to \n",
    "             write the random words.\n",
    "\n",
    "            num_of_words (int): How many words to generate.\n",
    "\n",
    "            selected_words (List[str]): Which words to use.\n",
    "        \"\"\"\n",
    "\n",
    "        with open(resource.filepath, 'w') as file_stream:\n",
    "            for i in range(num_of_words):\n",
    "                random_word = random.sample(selected_words, 1)[0]\n",
    "                file_stream.write(random_word)\n",
    "                file_stream.write(' ')\n",
    "\n",
    "    def load(self):\n",
    "        \"\"\"The `load()` method indicates to Shrkit that node is reentrant. \n",
    "        As `evaluate()` didn't return anything, we can leave it empty.        \n",
    "        \"\"\"\n",
    "        pass\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next step will be creating a new graph named `exp2` to hold our new experiment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp2 = srwf.get_graph('exp2', overwrite=True)    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `RandomText` is instantiated with the name `random_text`. It will generate the resource `'random-text.txt'`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp2.random_text = RandomText(srwf.FSResource('random-text.txt'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "According to logic of the interface, it accepts `num_of_words` as how many words to generate, and `selected_words` as which words to use. Let generate 50 words with the classic \"lorem ipsum\" text. The `with` shortcut will be used to set the `exp2.random_text.args` attributes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shrkit.workflow.util import is_eq_override"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "with exp2.random_text as args:\n",
    "    args.num_of_words = 50\n",
    "    args.selected_words = [\n",
    "            'lorem', 'ipsum', 'dolor', 'sit', 'amet',\n",
    "            'consectetur', 'adipiscing', 'elit', 'vestibulum',\n",
    "            'non', 'feugiat', 'felis']\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let connect the `random_text` node to a `WordFrequency` instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp2.word_freq = WordFrequency(srwf.FSResource('random-text-freq.txt'))\n",
    "exp2.word_freq.args.input_resource = exp2.random_text.resource"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In exp1, `word_freq` was direct assigned to `kmost_freq.args.word_freq`. In the above code, the assignment is between resource to another resource. Assigning a node's `.resource` is a special case which also generates dependency between nodes.\n",
    "\n",
    "Now reusing the `PrintKMost` viewing node:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp2.kmost_freq = PrintKMost()\n",
    "with exp2.kmost_freq as args:    \n",
    "    args.word_freq = exp2.word_freq\n",
    "    args.K = 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The final DAG becomes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graphviz.Source(viz.dag2dot(exp2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Executing it two times to check whatever Shrkit not execute `random_text` or `word_freq` without necessity."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp2.random_text.clear() # Remove previous run random text\n",
    "\n",
    "exp2.kmost_freq.call()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First time executed as expected, running all the DAG."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the redo flag again, to force\n",
    "# kmost_freq execution.\n",
    "exp2.kmost_freq.call(redo=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As expected, the system didn't evaluate two times `random_text` or `word_freq`, just `kmost_freq`.\n",
    "\n",
    "For testing raw python values changes, the next code modifies the random generator parameter to make 100 words:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp2.random_text.args.num_of_words = 100\n",
    "exp2.kmost_freq.call(redo=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When changing `num_of_words` to 100, Shrkit detected this and recalled all dependent nodes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Putting DAG inside workflow.py files\n",
    "\n",
    "Shrkit DAG can be used inside Jupyter notebooks, but it is primarily designed to run from files named `workflow.py`. This makes more comfortable sharing and reusing code. \n",
    "\n",
    "For the word counting example, create the file [workflow.py](workflow.py) in the same directory as the \"big.txt\" file.  Put all node definition code in it.\n",
    " \n",
    "The graph code will be the same, except that it should use the `shrkit.workflow.graph()` decorator to provide an initialized graph object. The next snippet shows how the graph code should be placed on the file:\n",
    "\n",
    "```python\n",
    "@srwf.graph()\n",
    "def exp2(g):\n",
    "    g.random_text = RandomText(srwf.FSResource('random-text.txt'))\n",
    "    with g.random_text as args:\n",
    "        args.num_of_words = 50\n",
    "        args.selected_words = [\n",
    "            'lorem', 'ipsum', 'dolor', 'sit', 'amet',\n",
    "            'consectetur', 'adipiscing', 'elit', 'vestibulum',\n",
    "            'non', 'feugiat', 'felis']\n",
    "\n",
    "    g.word_freq = WordFrequency(srwf.FSResource('random-text-freq.txt'))\n",
    "    g.word_freq.args.input_resource = g.random_text.resource\n",
    "\n",
    "    g.kmost_freq = PrintKMost()\n",
    "    with g.kmost_freq as args:    \n",
    "        args.word_freq = g.word_freq\n",
    "        args.K = 10\n",
    "```\n",
    "\n",
    "On the above code: \n",
    "\n",
    "* The graph's name is its function name - `exp2`.\n",
    "* The `g` variable is a already created graph by the decorator.\n",
    "\n",
    "Graphs then can be executed with `srwf` command. The first one will `clean` the `word_freq` node:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!srwf exp2 clean word_freq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first argument to `srwf` is the target graph. The second one is the command. There are the following commands:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!srwf exp2 -h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `run` command executes a given node. Use the  `-h` flag to show the available nodes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!srwf exp2 run -h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To run the `kmost_freq` from command-line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!srwf exp2 run kmost_freq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Testing again should not evaluate `word_freq` again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!srwf exp2 run kmost_freq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Graphviz visualizations are available on command line too:\n",
    "\n",
    "```shell\n",
    "$srwf exp2 viz-dag\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using sub-graphs\n",
    "\n",
    "Sometimes researchers want to keep two parameterized branches of an experiment. In Shrkit they can create sub-functions to generate on the fly.\n",
    "\n",
    "Suppose that we want to track the effects of counting 50 or 100 random words. Next code creates a function to generate DAGs with different word counts, through `word_count` parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def _sub_graph(s, word_count):\n",
    "    s.random_text = RandomText(srwf.FSResource('{}-random-text.txt'.format(word_count)))\n",
    "    with s.random_text as args:\n",
    "        args.num_of_words = word_count\n",
    "        args.selected_words = [\n",
    "            'lorem', 'ipsum', 'dolor', 'sit', 'amet',\n",
    "            'consectetur', 'adipiscing', 'elit', 'vestibulum',\n",
    "            'non', 'feugiat', 'felis']\n",
    "\n",
    "    s.word_freq = WordFrequency(srwf.FSResource('{}-random-text-freq.txt'.format(word_count)))\n",
    "    s.word_freq.args.input_resource = s.random_text.resource\n",
    "\n",
    "    s.kmost_freq = PrintKMost()\n",
    "    with s.kmost_freq as args:    \n",
    "        args.word_freq = s.word_freq\n",
    "        args.K = 10\n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** The resource file paths were set to reflect their corresponding parameters. \n",
    "\n",
    "There are two ways to use `_sub_graph`. The first is by creating a new graph:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp2_50 = srwf.get_graph('exp2_50', overwrite=True)\n",
    "exp2_100 = srwf.get_graph('exp2_100', overwrite=True)\n",
    "\n",
    "_sub_graph(exp2_50, 50)\n",
    "_sub_graph(exp2_100, 100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graphviz.Source(viz.dag2dot(exp2_50))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graphviz.Source(viz.dag2dot(exp2_100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, they can just be put in the same graph, but with different vertex names. The `prefix()` method returns a temporally graph, where each node has it name prefixed with the prefix string.  \n",
    "\n",
    "The following example creates a graph to hold 50 and 100 graphs. Each one prefixed with, respectively, `n50` and `n100` strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp2_50_100 = srwf.get_graph('exp2_50_100', overwrite=True)\n",
    "\n",
    "_sub_graph(exp2_50_100.prefix('n50'), 50)\n",
    "_sub_graph(exp2_50_100.prefix('n100'), 50)\n",
    "graphviz.Source(viz.dag2dot(exp2_50_100))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
