# Shrkit - Teste de uso do Framework

Antes da realização dos testes de usabilidade e preenchimento do
questionário, o participante precisa concordar e assinar o Termo de
Consentimento Livre e Esclarecido Impresso.

Este documento irá guia-lo durante o teste de usabilidade de um
*framework* de *workflows*, parte do projeto [*Shape Retrieval
Toolkit*](https://gitlab.com/shrkit/shape-retrieval-toolkit). Lembrando
que você tem o direito de abandonar este experimento a qualquer
momento, sem sofrer nenhum constrangimento por parte dos
pesquisadores.

O pesquisador responsável irá guia-los sobre o conteúdo deste
documento.

## Roteiro

Tempo estimado para realização do teste.

* Leitura da introdução: 7 minutos;
* Realização do tutorial: 15 minutos;
* Experimento: 1 hora e 30 minutos.

### Introdução

Leia [aqui](introducao.html).

### Tutorial

Por favor, leia com atenção o
[Tutorial](tutorial/full_tutorial.html) ou abra ele no
Jupyter Notebook (peça ajuda ao pesquisador). Sinta-se a vontade para
tirar qualquer dúvida com o pesquisador.

O sistema oferecido a você já vêm com o Shrkit instalado. 

### Experimento

O experimento avalia duas técnicas de extração de características
baseadas no operador de Laplace-Beltrami para recuperação de forma
3D. O conjunto de dados para a avaliação é o SHREC14[3]. As técnicas
são:

* ShapeDNA [1]
* Heat Kernel Signature (HKS) [2]

As duas ténicas são baseadas na analise espectral do operador de
Laplace-Beltrami aplicados a malhas tridimensionais discretas.

O Jupyter Notebook
[shapedna-experiment.ipynb](http://localhost:8888/notebooks/shapedna-experiment.ipynb)
contém um experimento feito sem o Shrkit para recuperação usando o
descritor ShapeDNA. Se o Jupyter Notebook ainda não está aberto,
execute:

```shell
trial$ jupyter notebook
```

Também se o diretório `SHREC14` não estiver disponível, download e
descomprima o
dataset. [Link](http://www.cs.cf.ac.uk/shaperetrieval/download.php).

Tente executar e entender esse experimento primeiro, e após tente
fazer as tarefas do questionário envolvendo o uso do Shrkit:

[Link para questionário no
Google](https://docs.google.com/forms/d/e/1FAIpQLSce2K6BT72RiFyEw3Se5YiKEY_jjb6sS-p77gAH5w3lHRq-pw/viewform?usp=sf_link)

O objetivo principal deste teste é avaliar a usabilidade do
*framework* para programadores, e não a qualidade dos
programadores. Sinta-se a vontade para programar do jeito que
preferir.

## Referências

[1] Reuter, Martin, Franz-Erich Wolter, and Niklas
Peinecke. "Laplace–Beltrami spectra as ‘Shape-DNA’of surfaces and
solids." Computer-Aided Design 38, no. 4 (2006): 342-366.

[2] Sun, Jian, Maks Ovsjanikov, and Leonidas Guibas. "A concise and
provably informative multi‐scale signature based on heat diffusion."
In Computer graphics forum, vol. 28, no. 5, pp. 1383-1392. Blackwell
Publishing Ltd, 2009.

[3] Pickup, D., Sun, X., Rosin, P.L., Martin, R.R., Cheng, Z., Lian,
Z., Aono, M., Hamza, A.B., Bronstein, A., Bronstein, M. and Bu,
S., 2014. SHREC’14 track: Shape retrieval of non-rigid 3D human
models. In Proceedings of the 7th Eurographics workshop on 3D Object
Retrieval (Vol. 1, No. 2, p. 6). Eurographics Association.
