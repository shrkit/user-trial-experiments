"""
"""
import shrkit.workflow as srwf
import shrkit.dataset.shrec14
import shrkit.feature.shapedna
import shrkit.feature.hks
from shrkit.evaluation import mean_average_precision, precision_at_k

from exp import (compute_lbos, load_lbos, compute_features,
                 create_ranker, run_queries,
                 InteractiveQuery, fit_bag_of_words, load_bag_of_words,
                 compute_codebook, load_codebook)

# pylint: disable=invalid-name, missing-docstring, no-self-use

class Dataset(srwf.Interface):
    def evaluate(self, dataset_path):
        return shrkit.dataset.shrec14.load_shrec14(dataset_path.filepath)


class LBO(srwf.Interface):
    def evaluate(self, resource, dataset):
        return compute_lbos(resource.filepath, dataset)

    def load(self, resource):
        return load_lbos(resource.filepath)


class Features(srwf.Interface):
    def evaluate(self, resource, lbos, extractor):
        return compute_features(resource.filepath, lbos, extractor)

    def load(self, resource):
        return load_lbos(resource.filepath)


class Ranker(srwf.Interface):
    def evaluate(self, feature_database, distance):
        return create_ranker(feature_database, distance)


class Queries(srwf.Interface):
    def evaluate(self, resource, ranker, features, top_k):
        queries = run_queries(resource.filepath, ranker,
                              features, top_k)
        resource.pickle_dump(queries)
        return queries

    def load(self, resource):
        return resource.pickle_load()


def _get_relevancy_matrix(query_rankings, dataset):
    # The `mean_average_precision` function accepts a list of
    # lists. Each row is different query, and columns are the
    # documents relevancy flag.
    relevancy_matrix = []

    # Constructs a dictionary to map rank model_id to its
    # category.
    modelid_to_category = dict(
        zip(dataset.model_id, dataset.category))

    for query_category, ranking in query_rankings:
        relevancy_matrix.append([query_category == modelid_to_category[rank.model_id]
                                 for rank in ranking])

    return relevancy_matrix


class MAPScore(srwf.Interface):
    """Calculates the Mean Average Precision (MAP) score. A ranked item is
    considered relevant if it have the same category as the query.
    """

    def evaluate(self, query_rankings, dataset):
        """Args:

            query_rankings ((int,
             List[:obj:`shrkit.ranking.Ranking`])): Ordered list
             (second tuple value) of entries returned by the ranking
             method, along with its ground-truth category (the first
             tuple value).

            retrieval_dataset (:obj:`pandas.DataFrame`): The parsed dataset content.

        """

        relevancy_matrix = _get_relevancy_matrix(query_rankings, dataset)
        map_value = mean_average_precision(relevancy_matrix)
        map_value = round(map_value, 2)

        self.save_measurement({'mAP': map_value})
        print("Mean Average Precision: {}".format(map_value))


class PrecisionAtK(srwf.Interface):
    """Calculates the Mean Average Precision (MAP) score. A ranked item is
    considered relevant if it have the same category as the query.
    """

    def evaluate(self, query_rankings, dataset, k):
        """Args:

            query_rankings ((int,
             List[:obj:`shrkit.ranking.Ranking`])): Ordered list
             (second tuple value) of entries returned by the ranking
             method, along with its ground-truth category (the first
             tuple value).

            retrieval_dataset (:obj:`pandas.DataFrame`): The parsed dataset content.

        """

        relevancy_matrix = _get_relevancy_matrix(query_rankings, dataset)
        precK_value = round(precision_at_k(relevancy_matrix, k), 2)

        self.save_measurement({'Precision@{}'.format(k): precK_value})
        print("Mean Average Precision: {}".format(precK_value))


@srwf.graph()
def shapedna(g):
    """Experiment graph using ShapeDNA features.
    """

    g.dataset = Dataset()
    g.dataset.args.dataset_path = srwf.FSResource("../SHREC14")

    g.lbo = LBO(srwf.FSResource("lbo.db"))
    g.lbo.args.dataset = g.dataset

    g.features = Features(srwf.FSResource("shapedna.db"))
    extractor = shrkit.feature.shapedna.ShapeDNA(spectrum_size=4)
    with g.features as args:
        args.lbos = g.lbo
        args.extractor = extractor

    def _create_rank_graph(s, distance):
        s.ranker = Ranker()
        s.ranker.show = False
        with s.ranker as args:
            args.feature_database = g.features
            args.distance = distance

        s.queries = Queries(srwf.FSResource("{}-queries.pkl".format(distance)))
        with s.queries as args:
            args.ranker = s.ranker
            args.features = g.features
            args.top_k = 100

        s.mAP_score = MAPScore()
        with s.mAP_score as args:
            args.query_rankings = s.queries
            args.dataset = g.dataset

        s.precK = PrecisionAtK()
        with s.precK as args:
            args.query_rankings = s.queries
            args.dataset = g.dataset
            args.k = 10

    _create_rank_graph(g.prefix("euc"), "euclidean")
    _create_rank_graph(g.prefix("cos"), "cosine")

    # g.interactive_rank = InteractiveQuery()
    # with g.interactive_rank as args:
    #     args.feature_extractor = g.feature_extractor
    #     args.ranker = g.create_ranker
    #     args.retrieval_dataset = g.dataset
    #     args.input_model = srwf.ProgramOption("--input-filepath")


class BagOfWords(srwf.Interface):
    def evaluate(self, resource, features, num_clusters, samples_per_obj):
        return fit_bag_of_words(resource.filepath, features, num_clusters, samples_per_obj)

    def load(self, resource):
        return load_bag_of_words(resource.filepath)


class CodeBook(srwf.Interface):
    def evaluate(self, resource, features, bow_model):
        return compute_codebook(resource.filepath, features, bow_model)

    def load(self, resource):
        return load_codebook(resource.filepath)


@srwf.graph()
def hks(g):
    sdna = srwf.open_graph('.', 'shapedna')

    extractor = shrkit.feature.hks.HKS(num_eigens=15)
    g.features = Features(srwf.FSResource('hks-features.db'))
    with g.features as args:
        args.extractor = extractor
        args.lbos = sdna.lbo

    g.bag_of_words = BagOfWords(srwf.FSResource('hks-bow-model.pkl'))
    with g.bag_of_words as args:
        args.features = g.features
        args.num_clusters = 64
        args.samples_per_obj = 3000

    g.codebook = CodeBook(srwf.FSResource('hks-codebook.db'))
    with g.codebook as args:
        args.features = g.features
        args.bow_model = g.bag_of_words

    g.ranker = Ranker()
    g.ranker.show = False
    with g.ranker as args:
        args.feature_database = g.codebook
        args.distance = 'cosine'

    g.queries = Queries(srwf.FSResource("hks-cosine-queries.pkl"))
    with g.queries as args:
        args.ranker = g.ranker
        args.features = g.codebook
        args.top_k = 100

    g.mAP_score = MAPScore()
    with g.mAP_score as args:
        args.query_rankings = g.queries
        args.dataset = sdna.dataset


if __name__ == '__main__':
    srwf.command.main()
