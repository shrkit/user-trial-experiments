# Teste de uso do Framework Tutorial

## O que é "3D Shape Retrieval"

Traduzido, Recuperação de Forma 3D, é a área que trata como buscar
formas 3D, como malhas poligonais ou volumes, a partir de um objeto
exemplo. Ou seja, dado um objeto 3D, os algoritmos dessa área retornam
uma lista dos modelos provenientes de um banco de dados ordenados por
similaridade à primeira entrada. Métodos de *shape retrieval* ajudam
artistas na composição de cenários tridimensionais, ou na organização
de bibliotecas de impressão 3D.

## O que é o Shrkit

Shrkit é um *framework* desenvolvido em Python para auxiliar na
elaboração de experimentos ligados a área. Ele prove uma API de
extratores de características e mecanismos para ordenar essas de
acordo com suas similaridades. Entretanto, a parte de fundamental
interesse neste teste é o seu modulo de *workflows* para processamento
de dados.

## O que são ferramentas de Workflow

*Frameworks* como PyTorch, TensorFlow e Caffe atacam especificamente o
problema de como treinar qualquer modelo de *Machine Learning* (ML)
por vez. Porém, algoritmos de recuperação 3D modernos aplicam
aprendizado de máquina em mais de um estágio. Além disso, para o
experimento seja realmente reproduzível, é necessário mais passos como
*download* de recursos, extração de descritores, divisão de conjuntos
de dados, e geração de modelos de ranqueamento (ordenação da
resposta). A missão da nossa ferramenta é prover uma camada para unir
esses passos de modo generalista.

Esta ideia não é nova. Muitos pesquisadores utilizam Make, Makeflow,
Weka e Spotify/Luigi para organizar seus fluxo experimentais. Neles,
os pesquisadores fazem os estágios salvarem resultados intermediários
em arquivos ou banco de dados, criando *checkpoints* e deixando o
experimento **reentrante**. Os resultados intermediários servem não só
para começar de onde parou, mas também na análise de possíveis erros
ou melhorias na tese sendo elaboradas.

O Shrkit tenta elevar isso ao um novo patamar. Nas ferramentas
existentes, o rastreamento de mudanças ocorre somente no nível de
arquivo ou banco de dados. Já nesse trabalho, objetos comuns em Python
também são verificados por mudanças. Isso elimina o chamado código *boilerplate*
de *parse* de parâmetros ou de serialização e desserialização.

## Por que desse teste

Uma das maiores apostas do Shrkit é a fácil utilização por parte dos
pesquisadores. O seu objetivo é eliminar o máximo de código não
estritamente ligando ao experimento.

Por meio desses testes será avaliado a percepção dos usuários e
melhorias na ferramenta.
