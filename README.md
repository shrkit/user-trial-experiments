User Trial Experiment
=====================

User Trial Process

* First, the index in [pt-BR](index.md).
* The participant first is conducted to the introduction presentation
  [pt-BR](introducao.md), done by a researcher.
* Then he/she is asked to read the tutorial
  [pt-BR](tutorial/tutorial.pt-BR.md).
* The workflow is presented [workflow.py](workflow.py).
* He/She access the [Survey link
  (pt-BR)](https://docs.google.com/forms/d/1ZbolzXYVJfzjXVqFtHE4DC4no03h3cNwaN9eljC9GCc)
  with the tasks.

The Researchers must:

* Assist the participants.
* Time how long participants take to finish each task.
