# Install pandoc:
# sudo apt install pandoc wkhtmltopdf

# --[ Tutorial
tutorial/full_tutorial.html: 
	make -C $(@D) $(@F)

tutorial/full_tutorial.ipynb: 
	make -C $(@D) $(@F)

# --[ Introduction
index.html: index.md
	pandoc index.md -o $@ --css=resources/github-pandoc.css --self-contained

index.pdf: index.html
	wkhtmltopdf $^ $@

introducao.html: introducao.md
	pandoc introducao.md -o $@ --css=resources/github-pandoc.css --self-contained

introducao.pdf: introducao.html
	wkhtmltopdf $^ $@

# --[ ShapeDNA experiment
clean.ipynb: shapedna-experiment-dev.ipynb
	jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace $^ 
	make -C tutorial.pt-BR clean.ipnyb

shapedna-experiment.ipynb: shapedna-experiment-dev.ipynb
	cp $^ $@
	jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace $@

shapedna-experiment-expected.ipynb: shapedna-experiment-dev.ipynb
	jupyter nbconvert --to html --execute --output $@ $^ --ExecutePreprocessor.timeout=-1

# --[ Trial Package
trial-package.zip: index.html introducao.html\
                   tutorial/full_tutorial.html\
				   tutorial/full_tutorial.ipynb\
				   tutorial/big-text.txt\
				   tutorial/workflow.py\
				   shapedna-experiment.ipynb\
				   explib/setup.py\
				   explib/exp/__init__.py explib/exp/_logic.py explib/exp/_interquery.py\
				   sample-obj.obj\
				   requirements.txt
	rm -f $@
	zip $@ $^

clean:
	make -C tutorial.pt-BR clean
	rm -rf trial-package.zip *.html *.pdf shapedna-experiment.ipynb *.db *.pkl

local-ci.trial_packages:
	gitlab-ci-multi-runner exec docker trial_package\
		--docker-pull-policy=never
