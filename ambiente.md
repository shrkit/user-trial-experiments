# Preparação do ambiente

Requisitos:

* Sistema operacional Ubuntu versões 14.04 até 17.10.

Instale as dependências do sistema operacional:

```shell
$ sudo apt-get install python3 python3-virtualenv git gcc libpython3-dev libeigen3-dev libglew-dev libglfw3-dev cmake git
```

Faça o download da última versão do arquivo `trial-package.zip` em:
https://gitlab.com/shrkit/user-trial-experiments/tags

Na shell faça:

```shell
$ mkdir -p trial-package
$ cd trial-package
trial-package$ unzip <path/para>/trial-package.zip
```

Crie um *virtual environment* para os testes, nesse exemplo será
usando um diretório `venv` dentro do próprio lugar clone de
`trial-package`

```shell
trial-package$ python3 -m venv venv
trial-package$ source venv/bin/activate
trial-package$ pip3 install -r requirements.txt
```

Faça o download do Dataset SHREC14 neste
[link](http://www.cs.cf.ac.uk/shaperetrieval/download.php). Descomprima
o arquivo SHREC14.zip no diretório shrec14:

```shell
trial-package$ cd shrec14
trial-package/shrec14$ unzip <path/para>/SHREC14.zip
```
